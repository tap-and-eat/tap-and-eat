import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  ParseIntPipe,
  Patch,
} from '@nestjs/common';
import { TableService } from './table.service';
import { CreateTableDto } from './dto/create-table.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { RestoGuard } from '../auth/guards/resto.guard';
import { Roles } from '../_shared_/decorators/role.decorator';
import { RestoDec } from 'src/_shared_/decorators/resto.decorator';
import { RestoPayload } from 'src/_shared_/interfaces';
import { UpdateTableDto } from './dto/update-table.dto';
import { UserRole } from '../auth/enums';
import { Table } from './entities/table.entity';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';
import { Order } from '../order/entities/order.entity';

@Controller('v1/tables')
@ApiSecurity('api_key', ['api_key'])
@ApiBearerAuth()
@UseGuards(RestoGuard, JwtGuard)
@ApiTags('Tables')
@ApiExtraModels(Table, Order)
export class TableController {
  constructor(private readonly tableService: TableService) {}

  @Post()
  @ApiCreatedResponse({ ...getGenericResponseSchema(Table) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  create(
    @RestoDec() resto: RestoPayload,
    @Body() createTableDto: CreateTableDto,
  ) {
    return this.tableService.create(resto, createTableDto);
  }

  @Get()
  @ApiOkResponse({ ...getArraySchema(Table) })
  findAll(@RestoDec() resto: RestoPayload) {
    return this.tableService.findAll(resto);
  }

  @Get(':number')
  @ApiOkResponse({ ...getGenericResponseSchema(Table) })
  findOne(
    @RestoDec() resto: RestoPayload,
    @Param('number', ParseIntPipe) number: string,
  ) {
    return this.tableService.findTable(resto, +number);
  }

  @Get(':number/orders')
  @ApiOkResponse({ ...getArraySchema(Order) })
  findTableOrders(
    @RestoDec() resto: RestoPayload,
    @Param('number', ParseIntPipe) number: string,
  ) {
    return this.tableService.findTableOrders(resto, +number);
  }
  @Get(':number/orders/unpaid')
  @ApiOkResponse({ ...getArraySchema(Order) })
  getLatestOrder(
    @RestoDec() resto: RestoPayload,
    @Param('number', ParseIntPipe) number: string,
  ) {
    return this.tableService.getUnpaidOrders(resto, +number);
  }

  @Patch(':number')
  @ApiOkResponse({ ...getGenericResponseSchema(Table) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  updateTable(
    @Param('number', ParseIntPipe) tableNumber: string,
    @Body() updateTableDto: UpdateTableDto,
  ) {
    return this.tableService.update(+tableNumber, updateTableDto);
  }

  @Delete(':number')
  @ApiOkResponse({ ...getGenericResponseSchema(null) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  remove(
    @RestoDec() resto: RestoPayload,
    @Param('number', ParseIntPipe) number: string,
  ) {
    return this.tableService.remove(resto, +number);
  }
}
