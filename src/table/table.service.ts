import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RestoPayload } from 'src/_shared_/interfaces';
import { FindOneOptions, Repository } from 'typeorm';
import { Order } from '../order/entities/order.entity';
import { sortStuffByDate } from '../_shared_/utils';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TableService {
  constructor(
    @InjectRepository(Table) private readonly tableRepo: Repository<Table>,
  ) {}

  async create(resto: RestoPayload, createTableDto: CreateTableDto) {
    const table = await this.tableRepo.findOne({
      where: { number: createTableDto.number, resto },
    });
    if (table) throw new ConflictException('Table already exists');
    const newTable = new Table();
    newTable.number = createTableDto.number;
    newTable.resto = resto as any;
    await this.tableRepo.save(newTable);
    return { data: newTable };
  }

  async findAll(resto: RestoPayload) {
    return {
      data: await this.tableRepo.find({
        where: { resto },
      }),
    };
  }
  async getUnpaidOrders(resto: RestoPayload, tableNumber: number) {
    const { data: table } = await this.findOne({
      where: { number: tableNumber, resto },
      relations: ['orders'],
    });
    const orders = sortStuffByDate<Order>(
      table.orders.filter((o) => o.isPaid === false),
    );
    return { data: { table: table.number, unpaid: orders } };
  }
  async findTable(resto: RestoPayload, number: number) {
    const { data: table } = await this.findOne({
      where: { number, resto },
      relations: ['orders'],
    });
    return { data: table };
  }
  async findTableOrders(resto: RestoPayload, tableNumber: number) {
    const { data: table } = await this.findOne({
      where: { number: tableNumber, resto },
      relations: ['orders'],
    });
    const orders = sortStuffByDate(table.orders);
    return {
      data: { table: table.number, orders },
    };
  }

  async update(tableNumber: number, updateTableDto: UpdateTableDto) {
    const table = await this.tableRepo.findOne({ number: tableNumber });
    if (!table) throw new NotFoundException('Table not found');
    table.number = updateTableDto.number;
    try {
      await this.tableRepo.save(table);
    } catch (error) {
      throw new ConflictException('This table number already exists');
    }
    return { message: 'Table updated', data: table };
  }

  async findOne(options: FindOneOptions<Table>) {
    const table = await this.tableRepo.findOne(options);
    if (!table) throw new NotFoundException('table not found');
    else return { data: table };
  }

  async remove(resto: RestoPayload, number: number) {
    await this.findOne({ where: { number, resto } });
    await this.tableRepo.delete({ number });
    return { message: 'Table deleted' };
  }

  async saveTable(tableInstance: Table): Promise<void> {
    await this.tableRepo.save(tableInstance);
  }
}
