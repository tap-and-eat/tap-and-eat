import { ApiProperty } from '@nestjs/swagger';
import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from '../../order/entities/order.entity';

@Entity('tables')
export class Table {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column({ nullable: false, unique: true })
  number: number;

  @OneToMany(() => Order, (order) => order.table)
  orders: Order[];

  @ManyToOne(() => Resto, (resto) => resto.tables)
  resto: Resto;
}
