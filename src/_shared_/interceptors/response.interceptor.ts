import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(ctx: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((results) => {
        const req = ctx.switchToHttp().getRequest();
        const res = ctx.switchToHttp().getResponse();
        if (req?.headers['user-agent'].startsWith('ESP')) return results?.text;
        return {
          status: res.statusCode,
          message: results?.message || 'Success',
          data: results.data || null,
          text: results?.text,
        };
      }),
    );
  }
}
