import { INestApplication } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';

const config = new DocumentBuilder()
  .addApiKey({
    type: 'apiKey',
    name: 'Resto-Token',
    in: 'header',
    description: 'Resto Token',
  })
  .addBearerAuth({
    type: 'http',
    scheme: 'bearer',
    description: 'Auth Token',
  })
  .setTitle('Tap and Eat')
  .setDescription('The Tap and Eat documentation')
  .setVersion('1.0.0')
  .addTag('App', 'App welcome endpoint')
  .addTag('Auth')
  .build();

const customOptions: SwaggerCustomOptions = {
  customSiteTitle: 'Tap and Eat API',
};

export function setupDocs(app: INestApplication): void {
  const document = SwaggerModule.createDocument(app, config);
  return SwaggerModule.setup('/api/swagger-ui', app, document, customOptions);
}
