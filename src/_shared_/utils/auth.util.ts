import { User } from '../../auth/entities/user.entity';

export const userSelectFields: (keyof User)[] = [
  'firstName',
  'lastName',
  'id',
  'role',
  'username',
  'active',
];
