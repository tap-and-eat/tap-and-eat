import { ApiProperty } from '@nestjs/swagger';
import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { OrderItem } from '../../order/entities/order-item.entity';
import { ItemStatus } from '../enums';
@Entity('items')
export class Item {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column({ nullable: false })
  name: string;

  @ApiProperty()
  @Column({ default: 'None' })
  description: string;

  @ApiProperty()
  @Column({ enum: ItemStatus, nullable: false, default: ItemStatus.AVAILABLE })
  status: ItemStatus;

  @ApiProperty()
  @Column({ nullable: false })
  price: number;

  @ApiProperty()
  @Column({ nullable: true })
  picture: string;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.item)
  orderedItems: OrderItem[];

  @ManyToOne(() => Resto, (resto) => resto.items)
  resto: Resto;
}
