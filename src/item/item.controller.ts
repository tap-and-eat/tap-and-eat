import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ItemService } from './item.service';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../_shared_/decorators/role.decorator';
import { RestoGuard } from 'src/auth/guards/resto.guard';
import { RestoDec } from 'src/_shared_/decorators/resto.decorator';
import { RestoPayload } from 'src/_shared_/interfaces';
import { UserRole } from '../auth/enums';
import { Item } from './entities/item.entity';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';

@Controller('v1/items')
@ApiSecurity('api_key', ['api_key'])
@ApiExtraModels(Item)
@ApiBearerAuth()
@ApiTags('Items')
@UseGuards(RestoGuard, JwtGuard)
export class ItemController {
  constructor(private readonly itemService: ItemService) {}

  @Post()
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  @ApiCreatedResponse({
    description: 'Item created',
    ...getGenericResponseSchema(Item),
  })
  create(
    @RestoDec() resto: RestoPayload,
    @Body() createItemDto: CreateItemDto,
  ) {
    return this.itemService.create(resto, createItemDto);
  }

  @Get()
  @ApiOkResponse({ ...getArraySchema(Item) })
  findAll(@RestoDec() resto: RestoPayload) {
    return this.itemService.findAll(resto);
  }
  @Get('search')
  @ApiOkResponse({ ...getArraySchema(Item) })
  search(@RestoDec() resto: RestoPayload, @Query('s') s: string) {
    return this.itemService.search(resto, s);
  }

  @Get(':id')
  @ApiOkResponse({ ...getGenericResponseSchema(Item) })
  findOne(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ) {
    return this.itemService.findOneItem(resto, id);
  }

  @Patch(':id')
  @ApiOkResponse({ ...getGenericResponseSchema(Item) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  @ApiOkResponse()
  update(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
    @Body() updateItemDto: UpdateItemDto,
  ) {
    return this.itemService.update(resto, id, updateItemDto);
  }

  @Delete(':id')
  @ApiOkResponse({ ...getGenericResponseSchema(null) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  @ApiOkResponse()
  remove(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ) {
    return this.itemService.remove(resto, id);
  }
}
