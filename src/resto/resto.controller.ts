import { Body, Controller, Get, Patch, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserRole } from '../auth/enums';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../_shared_/decorators/role.decorator';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';
import { DeactivateRestoDto } from './dto/deactivate-resto.dto';
import { Resto } from './entities/resto.entity';
import { RestoService } from './resto.service';

@Controller('v1/restos')
@ApiTags('Restos')
@ApiExtraModels(Resto)
@ApiBearerAuth()
@UseGuards(JwtGuard, RolesGuard)
export class RestoController {
  constructor(private readonly restoService: RestoService) {}
  @Get()
  @ApiOkResponse({ ...getArraySchema(Resto) })
  @Roles(UserRole.SITE_ADMIN)
  getRestos() {
    return this.restoService.getRestos(true);
  }
  @Get('/inactive')
  @ApiOkResponse({ ...getArraySchema(Resto) })
  @Roles(UserRole.SITE_ADMIN)
  getInactiveRestos() {
    return this.restoService.getRestos(false);
  }
  @Patch('deactivate')
  @ApiOkResponse({ ...getGenericResponseSchema(null) })
  @Roles(UserRole.SITE_ADMIN)
  deactivateResto(@Body() deactivateRestoDto: DeactivateRestoDto) {
    return this.restoService.deactivateResto(deactivateRestoDto);
  }
}
