import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRestoDto } from './dto/create-resto.dto';
import { Resto } from './entities/resto.entity';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { LoginRestoDto } from './dto/login-resto.dto';
import { RestoPayload } from 'src/_shared_/interfaces';
import { User } from 'src/auth/entities/user.entity';
import { DeactivateRestoDto } from './dto/deactivate-resto.dto';
import { UserRole } from '../auth/enums';

@Injectable()
export class RestoService {
  constructor(
    @InjectRepository(Resto) private restoRepo: Repository<Resto>,
    @InjectRepository(User) private userRepo: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}
  async createResto(createRestoDto: CreateRestoDto) {
    const resto = await this.restoRepo.findOne({
      where: { name: createRestoDto.name },
    });
    if (resto) throw new ConflictException('This resto already exists');
    let newResto: any = new Resto();
    newResto = {
      ...newResto,
      ...createRestoDto,
      password: await bcrypt.hash(createRestoDto.password, 10),
    };
    await this.restoRepo.save(newResto);
    delete newResto.password;

    const newRestoAdmin = new User();
    newRestoAdmin.firstName = 'Resto';
    newRestoAdmin.lastName = 'Admin';
    newRestoAdmin.username = newResto.name;
    newRestoAdmin.password = await bcrypt.hash(createRestoDto.password, 10);
    newRestoAdmin.role = UserRole.RESTO_ADMIN;
    newRestoAdmin.resto = newResto.id;
    await this.userRepo.save(newRestoAdmin);
    delete newRestoAdmin.password;
    return {
      message: 'Resto created',
      data: {
        resto_token: this.jwtService.sign({ ...newResto }),
        resto: newResto,
        restoAdmin: newRestoAdmin,
      },
    };
  }

  async loginResto(loginRestoDto: LoginRestoDto) {
    const resto = await this.restoRepo.findOne({
      where: { name: loginRestoDto.name, active: true },
    });
    if (!resto) throw new UnauthorizedException('Invalid resto credentials');
    const isMatch = await bcrypt.compare(
      loginRestoDto.password,
      resto.password,
    );
    if (!isMatch) throw new UnauthorizedException('Invalid resto credentials');
    delete resto.password;
    return {
      message: 'Resto Logged in',
      data: {
        resto_token: this.jwtService.sign({ ...resto }),
        restoData: resto,
      },
      text: this.jwtService.sign({ ...resto }),
    };
  }

  async validateResto(restoKey: string): Promise<Resto> {
    let decoded: RestoPayload;
    try {
      decoded = this.jwtService.verify(restoKey);
    } catch (error) {
      return;
    }
    if (!decoded) throw new UnauthorizedException('Invalid resto_token');

    const resto = await this.restoRepo.findOne({
      where: { id: decoded.id, name: decoded.name, active: true },
    });
    if (!resto) throw new UnauthorizedException('Invalid resto_token');
    delete resto.password;
    return resto;
  }

  async deactivateResto(deactivateRestoDto: DeactivateRestoDto) {
    const resto = await this.restoRepo.findOne({
      where: { name: deactivateRestoDto.name, active: true },
    });
    if (!resto) throw new NotFoundException('This resto does not exist');
    resto.active = false;
    await this.restoRepo.save(resto);
    return {
      message: 'Resto deactivated',
    };
  }

  async getRestos(active: boolean) {
    const restos = await this.restoRepo.find({
      where: { active },
      order: { name: 'ASC' },
    });
    return { message: 'Restos retrieved', data: restos };
  }

  async deleteResto(id: string) {
    const resto = await this.restoRepo.findOne({ active: false, id });
    if (!resto) throw new NotFoundException('Resto not found');
    await this.restoRepo.delete(resto);
    return { message: 'Resto deleted' };
  }
}
