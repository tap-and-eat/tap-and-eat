import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class DeactivateRestoDto {
  @IsString()
  @ApiProperty()
  name: string;
}
