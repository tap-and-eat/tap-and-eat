import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateRestoDto {
  @IsString()
  @ApiProperty()
  fullName: string;

  @IsString()
  @ApiProperty({ description: 'Unique resto username' })
  name: string;

  @IsString()
  @ApiProperty()
  password: string;
}
