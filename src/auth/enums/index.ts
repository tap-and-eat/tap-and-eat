export enum UserRole {
  SITE_ADMIN = 'SITE_ADMIN',
  RESTO_ADMIN = 'RESTO_ADMIN',
  MANAGER = 'MANAGER',
  WAITER = 'WAITER',
  KITCHEN = 'KITCHEN',
  AGENT = 'AGENT',
}

export enum RegisterRole {
  MANAGER = 'MANAGER',
  WAITER = 'WAITER',
  KITCHEN = 'KITCHEN',
}
