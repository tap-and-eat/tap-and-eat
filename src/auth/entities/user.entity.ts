import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Transaction } from 'src/card/entities/transaction.entity';
import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from '../../order/entities/order.entity';
import { UserRole } from '../enums';

@Entity('users')
export class User {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column()
  firstName: string;

  @ApiProperty()
  @Column()
  lastName: string;

  @ApiProperty()
  @Column()
  username: string;

  @Column()
  @Exclude()
  password: string;

  @ApiProperty()
  @Column()
  role: UserRole;

  @ApiProperty()
  @Column({ default: true })
  active: boolean;

  @OneToMany(() => Order, (order) => order.waiter)
  ordersServed: Order[];

  @ManyToOne(() => Resto, (resto) => resto.users)
  resto: Resto;

  @OneToMany(() => Transaction, (transaction) => transaction.agent)
  transactions: Transaction[];
}
