import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class DeactivateUserDto {
  @IsString()
  @ApiProperty()
  username: string;
}
