import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { RegisterRole, UserRole } from '../enums';

export class SignupDto {
  @IsString()
  @ApiProperty({ name: 'firstName' })
  firstName: string;
  @IsString()
  @ApiProperty({ name: 'lastName' })
  lastName: string;
  @IsString()
  @IsEnum(RegisterRole)
  @ApiProperty({ name: 'role', enum: RegisterRole })
  role: UserRole;
  @IsString()
  @ApiProperty({ name: 'username' })
  username: string;
  @IsString()
  @ApiProperty({ name: 'password' })
  password: string;
}
