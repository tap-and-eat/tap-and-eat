import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login-dto';
import { SignupDto } from './dto/signup-dto';
import { JwtGuard } from './guards/jwt.guard';
import { RolesGuard } from './guards/roles.guard';
import { DeactivateUserDto } from './dto/deactivate-user.dto';
import { Roles } from '../_shared_/decorators/role.decorator';
import { CreateRestoDto } from '../resto/dto/create-resto.dto';
import { RestoService } from '../resto/resto.service';
import { LoginRestoDto } from '../resto/dto/login-resto.dto';
import { RestoGuard } from './guards/resto.guard';
import { RestoDec } from 'src/_shared_/decorators/resto.decorator';
import { GenericResponse, RestoPayload } from 'src/_shared_/interfaces';
import { RegisterAgentDto } from './dto/register-agent.dto';
import { UserRole } from './enums';
import { User } from './entities/user.entity';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';

@ApiTags('Auth')
@ApiExtraModels(User)
@Controller('v1/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private restoService: RestoService,
  ) {}

  @Post('admin')
  adminLogin(@Body() loginDto: LoginDto): Promise<GenericResponse<any>> {
    return this.authService.adminLogin(loginDto);
  }

  @Post('login')
  @ApiSecurity('api_key', ['api_key'])
  @ApiConsumes('application/json', 'application/x-www-form-urlencoded')
  @UseGuards(RestoGuard)
  @HttpCode(HttpStatus.OK)
  logIn(@RestoDec() resto: RestoPayload, @Body() loginDto: LoginDto) {
    return this.authService.logIn(resto, loginDto);
  }

  @Post('login-resto')
  @HttpCode(HttpStatus.OK)
  @ApiConsumes('application/json', 'application/x-www-form-urlencoded')
  logInResto(@Body() loginRestoDto: LoginRestoDto) {
    return this.restoService.loginResto(loginRestoDto);
  }

  @Post('login-agent')
  @HttpCode(HttpStatus.OK)
  @ApiConsumes('application/json', 'application/x-www-form-urlencoded')
  logInAgent(@Body() loginDto: LoginDto) {
    return this.authService.loginAgent(loginDto);
  }

  @Post('register')
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @UseGuards(RestoGuard, JwtGuard, RolesGuard)
  @Roles(UserRole.RESTO_ADMIN)
  signUp(@RestoDec() resto: RestoPayload, @Body() signupDto: SignupDto) {
    return this.authService.signUp(resto, signupDto);
  }

  @Post('register-agent')
  @ApiBearerAuth()
  @UseGuards(JwtGuard, RolesGuard)
  @Roles(UserRole.SITE_ADMIN)
  registerAgent(@Body() registerAgentDto: RegisterAgentDto) {
    return this.authService.registerAgent(registerAgentDto);
  }

  @Post('register-resto')
  @ApiBearerAuth()
  @UseGuards(JwtGuard, RolesGuard)
  @Roles(UserRole.SITE_ADMIN)
  registerResto(@Body() createRestoDto: CreateRestoDto) {
    return this.restoService.createResto(createRestoDto);
  }

  @Get('users')
  @ApiOkResponse({ description: 'Users retrieved', ...getArraySchema(User) })
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @Roles(UserRole.RESTO_ADMIN)
  @UseGuards(JwtGuard, RestoGuard, RolesGuard)
  getUsers(@RestoDec() resto: RestoPayload) {
    return this.authService.getUsers(resto, null);
  }
  @Get('users/active')
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @ApiOkResponse({ description: 'Users retrieved', ...getArraySchema(User) })
  @Roles(UserRole.RESTO_ADMIN)
  @UseGuards(JwtGuard, RestoGuard, RolesGuard)
  getActiveUsers(@RestoDec() resto: RestoPayload) {
    return this.authService.getUsers(resto, true);
  }
  @Get('users/inactive')
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @ApiOkResponse({ description: 'Users retrieved', ...getArraySchema(User) })
  @Roles(UserRole.RESTO_ADMIN)
  @UseGuards(JwtGuard, RestoGuard, RolesGuard)
  getInactiveUsers(@RestoDec() resto: RestoPayload) {
    return this.authService.getUsers(resto, false);
  }

  @Get('users/:id')
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @ApiOkResponse({
    description: 'User retrieved',
    ...getGenericResponseSchema(User),
  })
  @Roles(UserRole.RESTO_ADMIN)
  @UseGuards(JwtGuard, RestoGuard, RolesGuard)
  getUserById(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) userId: string,
  ) {
    return this.authService.getUserById(resto, userId);
  }

  @Get('agents')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Agents retrieved', ...getArraySchema(User) })
  @Roles(UserRole.SITE_ADMIN)
  @UseGuards(JwtGuard, RolesGuard)
  getAgents() {
    return this.authService.getAgents(null);
  }
  @Get('agents/active')
  @ApiOkResponse({ description: 'Agents retrieved', ...getArraySchema(User) })
  @ApiBearerAuth()
  @Roles(UserRole.SITE_ADMIN)
  @UseGuards(JwtGuard, RolesGuard)
  getActiveAgents() {
    return this.authService.getAgents(true);
  }
  @Get('agents/inactive')
  @ApiOkResponse({ description: 'Agents retrieved', ...getArraySchema(User) })
  @ApiBearerAuth()
  @Roles(UserRole.SITE_ADMIN)
  @UseGuards(JwtGuard, RolesGuard)
  getInactiveAgents() {
    return this.authService.getAgents(false);
  }

  @Get('agents/:id')
  @ApiOkResponse({
    description: 'Agent retrieved',
    ...getGenericResponseSchema(User),
  })
  @ApiBearerAuth()
  @Roles(UserRole.SITE_ADMIN)
  @UseGuards(JwtGuard, RolesGuard)
  getAgentById(@Param('id', ParseIntPipe) userId: string) {
    return this.authService.getAgentById(userId);
  }
  @Patch('users/deactivate')
  @ApiOkResponse({
    description: 'User deactivated',
    ...getGenericResponseSchema(null),
  })
  @ApiBearerAuth()
  @ApiSecurity('api_key', ['api_key'])
  @Roles(UserRole.RESTO_ADMIN)
  @UseGuards(JwtGuard, RestoGuard, RolesGuard)
  deactivate(
    @RestoDec() resto: RestoPayload,
    @Body() deactivateUserDto: DeactivateUserDto,
  ) {
    return this.authService.deactivateUser(resto, deactivateUserDto);
  }

  @Patch('agents/deactivate')
  @ApiOkResponse({
    description: 'Agent deactivated',
    ...getGenericResponseSchema(null),
  })
  @ApiBearerAuth()
  @Roles(UserRole.SITE_ADMIN)
  @UseGuards(JwtGuard, RolesGuard)
  deactivateAgent(@Body() deactivateAgentDto: DeactivateUserDto) {
    return this.authService.deactivateAgent(deactivateAgentDto);
  }
}
