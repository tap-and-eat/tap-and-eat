import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Any, FindOneOptions, Repository } from 'typeorm';
import { adminAuth } from '../_shared_/config/env.config';
import { LoginDto } from './dto/login-dto';
import { User } from './entities/user.entity';
import {
  AuthResponse,
  GenericResponse,
  JwtPayload,
  RestoPayload,
} from '../_shared_/interfaces';
import { SignupDto } from './dto/signup-dto';
import { DeactivateUserDto } from './dto/deactivate-user.dto';
import { RegisterAgentDto } from './dto/register-agent.dto';
import { userSelectFields } from '../_shared_/utils/auth.util';
import { UserRole } from './enums';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private jwtService: JwtService,
  ) {}
  async adminLogin(loginDto: LoginDto): Promise<GenericResponse<AuthResponse>> {
    if (
      loginDto.username === adminAuth.username &&
      loginDto.password === adminAuth.password
    ) {
      const admin = await this.userRepo.findOne({
        where: { username: loginDto.username, role: UserRole.SITE_ADMIN },
      });
      if (admin) {
        const isMatch = await bcrypt.compare(loginDto.password, admin.password);
        if (!isMatch)
          throw new ForbiddenException(
            'You are trying to access a forbidden resource',
          );
        delete admin.password;
        return {
          data: {
            access_token: this.jwtService.sign({ ...admin }),
            userData: admin,
          },
        };
      }
      const newSiteAdmin = new User();
      newSiteAdmin.username = loginDto.username;
      const hash = await bcrypt.hash(loginDto.password, 10);
      newSiteAdmin.password = hash;
      newSiteAdmin.firstName = 'Pressme';
      newSiteAdmin.lastName = 'Admin';
      newSiteAdmin.role = UserRole.SITE_ADMIN;
      await this.userRepo.save(newSiteAdmin);
      delete newSiteAdmin.password;
      return {
        data: {
          access_token: this.jwtService.sign({ ...newSiteAdmin }),
          userData: newSiteAdmin,
        },
      };
    }
    throw new ForbiddenException(
      'You are trying to access a forbidden resource',
    );
  }

  async logIn(
    resto: RestoPayload,
    loginDto: LoginDto,
  ): Promise<GenericResponse<AuthResponse>> {
    const isAdmin =
      loginDto.username === adminAuth.username &&
      loginDto.password === adminAuth.password;
    const conditions: FindOneOptions<User> = isAdmin
      ? {
          where: { username: loginDto.username, active: true },
          relations: ['resto'],
        }
      : {
          where: { username: loginDto.username, active: true, resto },
          relations: ['resto'],
        };
    const user = await this.userRepo.findOne(conditions);
    if (!user) throw new UnauthorizedException('Invalid credentials');
    const isMatch = await bcrypt.compare(loginDto.password, user.password);
    if (!isMatch)
      throw new UnauthorizedException('Invalid username or password');
    delete user.password;
    delete user.resto?.password;
    return {
      message: 'Success',
      data: {
        access_token: this.jwtService.sign({ ...user }),
        userData: user,
      },
      text: this.jwtService.sign({ ...user }),
    };
  }

  /**
   * @Service Find and validate a user by username
   * @param payload
   * @returns Promise<User>
   */
  async validateUser(payload: JwtPayload): Promise<User> {
    const user = await this.userRepo.findOne({
      where: { username: payload.username },
    });
    if (!user) throw new UnauthorizedException('Invalid access_token');
    delete user.password;
    return user;
  }

  async signUp(
    resto: any,
    signupDto: SignupDto,
  ): Promise<GenericResponse<AuthResponse>> {
    if (signupDto.username === adminAuth.username)
      throw new ConflictException('That username exists');
    const user = await this.userRepo.findOne({
      where: { username: signupDto.username, resto: resto.id },
    });
    if (user) throw new ConflictException('User already exists');
    let newUser = new User();
    newUser = {
      ...newUser,
      ...signupDto,
      password: await bcrypt.hash(signupDto.password, 10),
    };
    newUser.resto = resto;
    await this.userRepo.save(newUser);
    delete newUser.password;
    return {
      data: {
        access_token: this.jwtService.sign({ ...newUser }),
        userData: newUser,
      },
    };
  }

  async registerAgent(
    registerAgentDto: RegisterAgentDto,
  ): Promise<GenericResponse<AuthResponse>> {
    if (registerAgentDto.username === adminAuth.username)
      throw new ConflictException('That username exists');
    const user = await this.userRepo.findOne({
      where: { username: registerAgentDto.username },
    });
    if (user) throw new ConflictException('Agent already exists');
    let newAgent = new User();
    newAgent = {
      ...newAgent,
      ...registerAgentDto,
      role: UserRole.AGENT,
      password: await bcrypt.hash(registerAgentDto.password, 10),
    };
    await this.userRepo.save(newAgent);
    delete newAgent.password;
    return {
      data: {
        access_token: this.jwtService.sign({ ...newAgent }),
        userData: newAgent,
      },
    };
  }

  async loginAgent(loginDto: LoginDto): Promise<GenericResponse<AuthResponse>> {
    const agent = await this.userRepo.findOne({
      where: { username: loginDto.username, role: UserRole.AGENT },
    });
    if (!agent) throw new UnauthorizedException('Invalid credentials');
    const isMatch = await bcrypt.compare(loginDto.password, agent.password);
    if (!isMatch)
      throw new UnauthorizedException('Invalid username or password');
    delete agent.password;
    return {
      message: 'Success',
      data: {
        access_token: this.jwtService.sign({ ...agent }),
        userData: agent,
      },
      text: this.jwtService.sign({ ...agent }),
    };
  }
  async deactivateUser(
    resto: RestoPayload,
    deactivateDto: DeactivateUserDto,
  ): Promise<GenericResponse<null>> {
    const user = await this.userRepo.findOne({
      where: {
        username: deactivateDto.username,
        resto: resto.id,
        active: true,
        role: Any([UserRole.WAITER, UserRole.KITCHEN, UserRole.MANAGER]),
      },
    });
    if (!user) throw new NotFoundException('User not found');
    user.active = false;
    await this.userRepo.save(user);
    return { message: 'User deactivated' };
  }

  async getUsers(
    resto: RestoPayload,
    active: boolean | null,
  ): Promise<GenericResponse<User[]>> {
    let users: User[];
    if (active === null)
      users = await this.userRepo.find({
        where: {
          resto,
          role: Any([UserRole.WAITER, UserRole.KITCHEN, UserRole.MANAGER]),
        },
        select: [...userSelectFields],
        order: { username: 'ASC' },
      });
    else
      users = await this.userRepo.find({
        where: {
          resto,
          active,
          role: Any([UserRole.WAITER, UserRole.KITCHEN, UserRole.MANAGER]),
        },
        select: [...userSelectFields],
        order: { username: 'ASC' },
      });
    return {
      message: 'Users retrieved',
      data: users,
    };
  }

  async getUserById(
    resto: RestoPayload,
    id: string,
  ): Promise<GenericResponse<User>> {
    const user = await this.userRepo.findOne({
      where: {
        id,
        resto,
        role: Any([UserRole.WAITER, UserRole.KITCHEN, UserRole.MANAGER]),
      },
      select: [...userSelectFields],
      relations: ['ordersServed', 'transactions'],
    });
    if (!user) throw new NotFoundException('User not found');
    return { message: 'User retrieved', data: user };
  }

  async deleteUser(id: string): Promise<GenericResponse<null>> {
    const user = await this.userRepo.findOne({
      where: {
        id,
        active: false,
        role: Any([UserRole.WAITER, UserRole.KITCHEN, UserRole.MANAGER]),
      },
    });
    if (!user) throw new NotFoundException('User not found');
    await this.userRepo.delete(user);
    return { message: 'User deleted' };
  }
  async deactivateAgent(
    deactivateAgentDto: DeactivateUserDto,
  ): Promise<GenericResponse<null>> {
    const agent = await this.userRepo.findOne({
      where: {
        role: UserRole.AGENT,
        username: deactivateAgentDto.username,
        active: true,
      },
    });
    if (!agent) throw new NotFoundException('This agent does not exist');
    agent.active = false;
    await this.userRepo.save(agent);
    return {
      message: 'Agent deactivated',
    };
  }

  async getAgents(active: boolean | null): Promise<GenericResponse<User[]>> {
    let agents: User[];
    if (active === null) {
      agents = await this.userRepo.find({
        where: { role: UserRole.AGENT },
        order: { username: 'ASC' },
        select: [...userSelectFields],
      });
    } else {
      agents = await this.userRepo.find({
        where: { active, role: UserRole.AGENT },
        order: { username: 'ASC' },
      });
    }
    return {
      message: 'Agents retrieved',
      data: agents,
    };
  }
  async getAgentById(id: string): Promise<GenericResponse<User>> {
    const agent = await this.userRepo.findOne({
      where: { id, role: UserRole.AGENT },
      select: [...userSelectFields],
    });
    if (!agent) throw new NotFoundException('Agent not found');
    return {
      message: 'Agent retrieved',
      data: agent,
    };
  }

  async deleteAgent(id: string): Promise<GenericResponse<null>> {
    const agent = await this.userRepo.findOne({
      active: false,
      id,
      role: UserRole.AGENT,
    });
    if (!agent) throw new NotFoundException('Agent not found');
    await this.userRepo.delete(agent);
    return { message: 'Agent deleted' };
  }
}
