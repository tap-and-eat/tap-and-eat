import { ApiProperty } from '@nestjs/swagger';
import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Transaction } from './transaction.entity';

@Entity('cards')
export class Card {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column()
  owner: string;

  @ApiProperty()
  @Column()
  uid: string;

  @ApiProperty()
  @Column({ default: 0 })
  balance: number;

  @ApiProperty({ type: () => Array })
  @OneToMany(() => Transaction, (transaction) => transaction.card)
  transactions: Transaction[];

  @ManyToOne(() => Resto, (resto) => resto.cards)
  resto: Resto;
}
