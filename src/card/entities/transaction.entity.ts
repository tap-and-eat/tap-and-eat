import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/auth/entities/user.entity';
import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TransactionType } from '../enums';
import { Card } from './card.entity';

@Entity('transactions')
export class Transaction {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column()
  cardUid: string;

  @ApiProperty()
  @Column()
  type: TransactionType;

  @ApiProperty()
  @Column()
  amount: number;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  date: Date;

  @ApiProperty({ type: () => Card })
  @ManyToOne(() => Card, (card) => card.transactions)
  card: Card;
  @ManyToOne(() => User, (agent) => agent.transactions)
  agent: User;
  @ManyToOne(() => Resto, (resto) => resto.cardTransactions)
  resto: Resto;
}
