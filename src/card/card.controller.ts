import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from 'src/_shared_/decorators/role.decorator';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { CardService } from './card.service';
import { CreateCardDto } from './dto/create-card.dto';
import { UserData } from 'src/_shared_/decorators/user.decorator';
import { JwtPayload, RestoPayload } from 'src/_shared_/interfaces';
import { RestoGuard } from 'src/auth/guards/resto.guard';
import { RestoDec } from 'src/_shared_/decorators/resto.decorator';
import { UserRole } from '../auth/enums';
import { Card } from './entities/card.entity';
import { Transaction } from './entities/transaction.entity';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';

@Controller('v1/cards')
@UseGuards(JwtGuard, RolesGuard)
@ApiSecurity('api_key', ['api_key'])
@ApiBearerAuth()
@ApiTags('Cards')
@ApiExtraModels(Card, Transaction)
@Roles(UserRole.AGENT)
export class CardController {
  constructor(private readonly cardService: CardService) {}

  @Post()
  @ApiCreatedResponse({ ...getGenericResponseSchema(Card) })
  @Roles(UserRole.AGENT)
  create(@Body() createCardDto: CreateCardDto) {
    return this.cardService.create(createCardDto);
  }

  @Get()
  @ApiOkResponse({ ...getArraySchema(Card) })
  findAll() {
    return this.cardService.findAll();
  }

  @Get('transactions/all')
  @ApiOkResponse({ ...getArraySchema(Transaction) })
  findAllTransactions() {
    return this.cardService.findAllTransactions();
  }

  @Get('transactions')
  @ApiOkResponse({ ...getArraySchema(Transaction) })
  @UseGuards(RestoGuard)
  @Roles(UserRole.MANAGER, UserRole.RESTO_ADMIN)
  findRestoTransactions(@RestoDec() resto: RestoPayload) {
    return this.cardService.findRestoTransactions(resto);
  }

  @Get(':uid')
  @ApiOkResponse({ ...getGenericResponseSchema(Card) })
  findOne(@Param('uid') uid: string) {
    return this.cardService.findOneCard(uid);
  }

  @Get(':uid/transactions')
  @ApiOkResponse({ ...getArraySchema(Transaction) })
  findSingleCardTransactions(@Param('uid') uid: string) {
    return this.cardService.findSingleCardTransactions(uid);
  }

  @Patch(':uid/recharge')
  rechargeBalance(
    @UserData() user: JwtPayload,
    @Param('uid') uid: string,
    @Query('amount', ParseIntPipe) amount: string,
  ) {
    return this.cardService.rechargeBalance(user, uid, +amount);
  }

  @Delete(':uid')
  @ApiOkResponse({ ...getGenericResponseSchema(null) })
  remove(@Param('uid') uid: string) {
    return this.cardService.remove(uid);
  }
}
