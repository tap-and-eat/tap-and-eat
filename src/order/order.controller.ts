import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { OrderService } from './order.service';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiQuery,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Order } from './entities/order.entity';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { UserData } from '../_shared_/decorators/user.decorator';
import { JwtPayload, RestoPayload } from '../_shared_/interfaces';
import { CreateOrderDto } from './dto/create-order.dto';
import { Roles } from '../_shared_/decorators/role.decorator';
import { OrderStatus } from '../order/enums';
import { UserRole } from '../auth/enums';
import { RestoDec } from 'src/_shared_/decorators/resto.decorator';
import { RestoGuard } from 'src/auth/guards/resto.guard';
import {
  getArraySchema,
  getGenericResponseSchema,
} from '../_shared_/utils/swagger.util';

@Controller('v1/orders')
@ApiTags('Orders')
@ApiExtraModels(Order)
@ApiSecurity('api_key', ['api_key'])
@ApiBearerAuth()
@UseGuards(RestoGuard, JwtGuard)
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Get()
  @ApiOkResponse({ ...getArraySchema(Order) })
  @ApiQuery({ name: 'status', required: false, enum: OrderStatus })
  getAll(
    @RestoDec() resto: RestoPayload,
    @Query('status') status?: OrderStatus,
  ) {
    return this.orderService.findAllOrders(resto, status);
  }
  /**
   *
   * @param query tableNumber
   * @returns Order
   */
  @Post('table/:tableNumber')
  @ApiCreatedResponse({ ...getGenericResponseSchema(Order) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.WAITER, UserRole.RESTO_ADMIN)
  create(
    @RestoDec() resto: RestoPayload,
    @UserData() user: JwtPayload,
    @Param('tableNumber', ParseIntPipe) tableNumber: string,
    @Body() createOrderDto: CreateOrderDto,
  ) {
    return this.orderService.create(resto, user, +tableNumber, createOrderDto);
  }

  /**
   *
   * @param query tableId
   * @returns Order[]
   */
  @Get('table/:tableNumber')
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  findAll(
    @RestoDec() resto: RestoPayload,
    @Param('tableNumber', ParseIntPipe) tableNumber: string,
  ) {
    return this.orderService.findAll(resto, +tableNumber);
  }

  /**
   *
   * @returns Order[]
   */
  @Get('paid-confirmed')
  @ApiOkResponse({ ...getArraySchema(Order) })
  findPaidConfirmed(@RestoDec() resto: RestoPayload) {
    return this.orderService.findPaidConfirmed(resto);
  }

  @Get(':id')
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  findOne(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ) {
    return this.orderService.findOne({
      where: { id, resto },
      relations: ['orderItems', 'table'],
    });
  }

  @Patch(':id/confirm')
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.WAITER, UserRole.RESTO_ADMIN)
  confirm(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ): Promise<{ data: Order }> {
    return this.orderService.confirm(resto, id);
  }

  @Patch('pay')
  @UseGuards(RolesGuard)
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  @Roles(UserRole.WAITER, UserRole.RESTO_ADMIN)
  pay(
    @RestoDec() resto: RestoPayload,
    @UserData() user: JwtPayload,
    @Query('uid') uid: string,
  ): Promise<any> {
    return this.orderService.pay(resto, user, uid);
  }

  @Patch(':id/finish')
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.KITCHEN, UserRole.RESTO_ADMIN)
  finish(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ): Promise<{ data: Order }> {
    return this.orderService.finish(resto, id);
  }

  @Patch(':id/serve')
  @ApiOkResponse({ ...getGenericResponseSchema(Order) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.KITCHEN, UserRole.RESTO_ADMIN)
  serve(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ): Promise<{ data: Order }> {
    return this.orderService.serve(resto, id);
  }

  @Delete(':id')
  @ApiOkResponse({ ...getGenericResponseSchema(null) })
  @UseGuards(RolesGuard)
  @Roles(UserRole.WAITER, UserRole.RESTO_ADMIN)
  remove(
    @RestoDec() resto: RestoPayload,
    @Param('id', ParseIntPipe) id: string,
  ) {
    return this.orderService.delete(resto, id);
  }
}
