import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as prettyMilliseconds from 'pretty-ms';
import { Card } from 'src/card/entities/card.entity';
import { Transaction } from 'src/card/entities/transaction.entity';
import { JwtPayload, RestoPayload } from 'src/_shared_/interfaces';
import { Any, FindOneOptions, Repository } from 'typeorm';
import { Item } from '../item/entities/item.entity';
import { OrderItem } from './entities/order-item.entity';
import { TableService } from '../table/table.service';
import { orderPrice } from '../_shared_/utils';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from './entities/order.entity';
import { OrderStatus } from './enums';
import { TransactionType } from '../card/enums';
import { AppGateway } from '../app.gateway';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order) private readonly orderRepo: Repository<Order>,
    @InjectRepository(Card) private readonly cardRepo: Repository<Card>,
    @InjectRepository(Transaction)
    private readonly transactionRepo: Repository<Transaction>,
    @InjectRepository(OrderItem)
    private readonly orderItemRepo: Repository<OrderItem>,
    @InjectRepository(Item)
    private readonly itemRepo: Repository<Item>,
    private readonly tableService: TableService,
    private readonly gateway: AppGateway,
  ) {}
  async create(
    resto: RestoPayload,
    user: JwtPayload,
    tableNumber: number,
    createOrderDto: CreateOrderDto,
  ) {
    const { data: table } = await this.tableService.findOne({
      where: { number: tableNumber, resto },
    });
    const unpaidOrder = await this.orderRepo.findOne({
      where: { waiter: user, isPaid: false },
    });
    if (unpaidOrder) {
      const isExpired = await this.checkOrderForExpiry(unpaidOrder);
      if (!isExpired)
        throw new BadRequestException(
          'There is still an unpaid order from this waiter',
        );
    }

    let newOrder = new Order();
    newOrder = { ...newOrder, ...createOrderDto };
    newOrder.table = table;
    newOrder.status = OrderStatus.PENDING;
    newOrder.waiter = user as any;
    newOrder.resto = resto as any;
    newOrder = await this.orderRepo.save(newOrder);
    for (const it of createOrderDto.items) {
      const item = await this.itemRepo.findOne(it.itemId);
      const newOrderItem = new OrderItem();
      newOrderItem.item = it.itemId as any;
      newOrderItem.order = newOrder.id as any;
      newOrderItem.name = item.name;
      newOrderItem.quantity = it.quantity;
      newOrderItem.price = item.price * it.quantity;
      newOrderItem.resto = resto as any;
      await this.orderItemRepo.save(newOrderItem);
    }
    newOrder = await this.orderRepo.findOne({
      where: { id: newOrder.id },
      relations: ['orderItems'],
    });
    newOrder.price = orderPrice(newOrder.orderItems);
    await this.orderRepo.save(newOrder);
    return { data: newOrder };
  }

  async findAllOrders(resto: RestoPayload, status: OrderStatus) {
    let orders: Order[];
    if (!status)
      orders = await this.orderRepo.find({
        where: { resto },
        order: { dateCreated: 'DESC' },
      });
    else
      orders = await this.orderRepo.find({
        where: { resto, status },
        order: { dateCreated: 'DESC' },
      });
    return {
      message: 'Success',
      data: orders,
    };
  }

  async findAllOrderItems(resto: RestoPayload) {
    return {
      message: 'Success',
      data: await this.orderItemRepo.find({
        where: { resto },
      }),
    };
  }

  async confirm(resto: RestoPayload, orderId: string) {
    const { data: order } = await this.findOne({
      where: { id: orderId, resto, isPaid: false, status: OrderStatus.PENDING },
    });
    const isExpired = await this.checkOrderForExpiry(order);
    if (isExpired) throw new BadRequestException('This order has expired');
    order.status = OrderStatus.CONFIRMED;
    order.timeConfirmed = new Date();
    await this.orderRepo.save(order);
    return { data: order };
  }

  async pay(resto: RestoPayload, user: JwtPayload, uid: string) {
    const card = await this.cardRepo.findOne({ where: { uid } });
    this.emitError('Payment failed', 'This card does not exist');
    if (!card) throw new NotFoundException('This card does not exist');
    const orders = await this.orderRepo.find({
      where: {
        resto,
        waiter: user,
        isPaid: false,
        status: OrderStatus.CONFIRMED,
      },
      order: { dateCreated: 'DESC' },
    });
    const order = orders[0];
    Logger.log({ ...order }, 'AAAA');
    this.emitError('Payment failed', 'This order does not exist');
    if (!order) throw new NotFoundException('This order does not exist');
    const isExpired = await this.checkOrderForExpiry(order);
    this.emitError('Payment failed', 'This order has expired');
    if (isExpired) throw new BadRequestException('This order has expired');
    if (card.balance < order.price) {
      this.emitError(
        'Payment failed',
        `Balance of ${card.balance} Not enough for this transaction`,
      );
      throw new BadRequestException(
        `Balance of ${card.balance} Not enough for this transaction`,
      );
    }
    card.balance = card.balance - order.price;
    order.paidPrice = order.price;
    order.isPaid = true;
    const newTransaction = new Transaction();
    newTransaction.type = TransactionType.PAYMENT;
    newTransaction.amount = order.paidPrice;
    newTransaction.card = card;
    newTransaction.cardUid = uid;
    newTransaction.resto = resto as any;
    await this.orderRepo.save(order);
    await this.cardRepo.save(card);
    await this.transactionRepo.save(newTransaction);
    delete order.table;
    this.emitSuccess('Payment successful', {
      order,
      paid: order.paidPrice,
      balance: card.balance,
    });
    return {
      message: 'Payment successful',
      data: { order, paid: order.paidPrice, balance: card.balance },
      text: `Success. Balance: ${card.balance}`,
    };
  }
  async finish(resto: RestoPayload, orderId: string) {
    let { data: order } = await this.findOne({
      where: {
        id: orderId,
        resto,
        isPaid: true,
        status: OrderStatus.CONFIRMED,
      },
    });
    order.status = OrderStatus.FINISHED;
    order.timeConfirmedToServed = prettyMilliseconds(
      new Date().getTime() - order.timeConfirmed.getTime(),
    );
    order = await this.orderRepo.save(order);
    return { data: order };
  }

  async serve(resto: RestoPayload, orderId: string) {
    let { data: order } = await this.findOne({
      where: {
        id: orderId,
        resto,
        isPaid: true,
        status: OrderStatus.FINISHED,
      },
    });
    order.status = OrderStatus.SERVED;
    order.timeConfirmedToServed = prettyMilliseconds(
      new Date().getTime() - order.timeConfirmed.getTime(),
    );
    order = await this.orderRepo.save(order);
    return { data: order };
  }

  async findAll(resto: RestoPayload, tableNumber: number | undefined) {
    if (!tableNumber)
      return {
        data: await this.orderRepo.find({
          where: { resto },
          relations: ['table'],
        }),
      };
    const { data: table } = await this.tableService.findTable(
      resto,
      tableNumber,
    );
    return {
      data: await this.orderRepo.find({
        where: { table },
        order: { dateCreated: 'DESC' },
      }),
    };
  }

  async findPaidConfirmed(resto: RestoPayload) {
    return {
      data: await this.orderRepo.find({
        where: { resto, isPaid: true, status: OrderStatus.CONFIRMED },
        relations: ['table', 'waiter', 'orderItems'],
        order: { dateCreated: 'DESC' },
      }),
    };
  }

  async findOne(options?: FindOneOptions<Order>) {
    const order = await this.orderRepo.findOne(options);
    if (!order) throw new NotFoundException('order not found');
    else return { data: order };
  }

  async delete(resto: RestoPayload, id: string) {
    await this.findOne({
      where: {
        id,
        resto,
        isPaid: false,
        status: Any([OrderStatus.PENDING, OrderStatus.CONFIRMED]),
      },
    });
    await this.orderRepo.delete(id);
    return { message: 'Order deleted' };
  }

  async saveOrder(orderInstance: Order): Promise<void> {
    await this.orderRepo.save(orderInstance);
  }

  async checkOrderForExpiry(order: Order): Promise<boolean> {
    if (order) {
      if (new Date().getTime() - order.dateCreated.getTime() >= 120000) {
        await this.orderRepo.delete(order.id);
        return true;
      } else {
        return false;
      }
    }
  }

  private emitError(message: string, error: any): void {
    this.gateway.server.emit('payment_failed', {
      message,
      error,
    });
    return;
  }
  private emitSuccess(message: string, data: any): void {
    this.gateway.server.emit('payment_success', {
      message,
      data,
    });
    return;
  }
}
