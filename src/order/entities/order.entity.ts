import { Resto } from 'src/resto/entities/resto.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { OrderItem } from './order-item.entity';
import { Table } from '../../table/entities/table.entity';
import { OrderStatus, PaymentMethod } from '../enums';
import { ApiProperty } from '@nestjs/swagger';
@Entity('orders')
export class Order {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: string;

  @ApiProperty()
  @Column({ default: 0 })
  price: number;

  @ApiProperty({ default: false })
  @Column({ default: false })
  isPaid: boolean;

  @ApiProperty()
  @Column({ default: 0 })
  paidPrice: number;

  @ApiProperty()
  @Column({ enum: OrderStatus, default: OrderStatus.PENDING, nullable: false })
  status: OrderStatus;

  @ApiProperty()
  @Column({ nullable: true })
  timeConfirmed: Date;

  @ApiProperty()
  @Column({ enum: PaymentMethod, nullable: true })
  paymentMethod: PaymentMethod;
  /**
  /**
   * time when finished - confirmed time
   */
  @ApiProperty()
  @Column({ nullable: true })
  timeConfirmedToServed: string;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  dateCreated: Date;

  @ManyToOne(() => Table, (table) => table.orders)
  table: Table;

  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, (user) => user.ordersServed)
  waiter: User;

  @ApiProperty({ type: () => Array })
  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @ManyToOne(() => Resto, (resto) => resto.orders)
  resto: Resto;
}
