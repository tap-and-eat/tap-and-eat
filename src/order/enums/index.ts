export enum OrderStatus {
  PENDING = 'PENDING',
  CONFIRMED = 'CONFIRMED',
  FINISHED = 'FINISHED',
  SERVED = 'SERVED',
}

export enum PaymentMethod {
  MOBILE_MONEY = 'MOBILE_MONEY',
  TAP_CARD = 'TAP_CARD',
  CREDIT_CARD = 'CREDIT_CARD',
  CASH = 'CASH',
}
