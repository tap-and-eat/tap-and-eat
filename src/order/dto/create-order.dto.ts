import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEnum } from 'class-validator';
import { PaymentMethod } from '../enums';

interface OrderItemQty {
  itemId: string;
  quantity: number;
}

export class CreateOrderDto {
  @ApiProperty({
    enum: PaymentMethod,
    required: false,
    default: PaymentMethod.MOBILE_MONEY,
  })
  @IsEnum(PaymentMethod)
  paymentMethod: PaymentMethod;

  @IsArray()
  @ApiProperty({
    default: [
      {
        itemId: '1',
        quantity: 5,
      },
      {
        itemId: '2',
        quantity: 3,
      },
    ],
  })
  items: OrderItemQty[];
}
